from azure.identity import ClientSecretCredential
from azure.mgmt.containerservice import ContainerServiceClient
from kubernetes import client, config
import yaml
import json
import os
import helper 
from exception_handling import log_exception 
import configuration as GitlabConfigConnection
import datetime
cicdGitlabConfig = GitlabConfigConnection.GitlabConfig
 

@log_exception()
def GetAKSNamespaceDetails(): 
    # get AKS client object 
    aks_client = GetAKSClient() 

    # read JSON config file from secured file location
    pipeline_configuration = helper.ReadJSONFileFromCurrentDirectory(Filelocation="pipeline_configuration.json")
    
    # Get all clusters in the resource group
    clusters = pipeline_configuration[cicdGitlabConfig.NODE_AZURE_AKS_LIST]
    valueOfBillingConfig = pipeline_configuration[cicdGitlabConfig.NODE_VALUE_OF_BILLING]
    
    # Iterate over each cluster
    ExtractAllNamespacesAndLabels(aks_client, clusters, valueOfBillingConfig)

compline_namespaces_json = []

@log_exception()
def ExtractAllNamespacesAndLabels(aks_client, clusters, valueOfBillingConfig):
    for cluster in clusters:
         # Initialize an empty list to store cluster and namespace details
        
        raw_namespace_json =[]

        # Get the access list cluster user credentials for the cluster
        access_profile = aks_client.managed_clusters.list_cluster_user_credentials(
            os.environ.get(cicdGitlabConfig.VAR_AKS_RESOURCE_GROUP),
            cluster[cicdGitlabConfig.NODE_AKS_NAME] 
        )
        # # Extract the kubeconfig file content from the access profile
        kube_config_bytes = access_profile.kubeconfigs[0].value
        kube_config_str = kube_config_bytes.decode("utf-8")
        kube_config = yaml.safe_load(kube_config_str)
         # Get the current directory path
        current_dir = os.getcwd()

        # Construct the file path by joining the current directory path and the Filelocation
        file_path = os.path.join(current_dir, "\customkubeconfig") 
        

        with open(file_path, 'w') as file:
            json.dump(kube_config, file)

        # Define the command
        VAR_AZURE_CLIENT_ID = os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_ID)
        VAR_AZURE_CLIENT_SECRET= os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_SECRET)
         
        import subprocess
        command = [
            "kubelogin",
            "convert-kubeconfig",
            "-l",
            "spn",
            "--client-id",
            VAR_AZURE_CLIENT_ID,
            "--client-secret",
            VAR_AZURE_CLIENT_SECRET,
            "--kubeconfig",
            file_path
        ]
        # Execute the command 
        subprocess.run(command, check=True)

        # Load kubeconfig directly from dictionary
        config.load_kube_config(config_file=file_path)
#        config.kube_config.load_kube_config_from_dict(kube_config)

        # Check if the file exists
        if os.path.exists(file_path):
            # Delete the file
            os.remove(file_path)
            print("File deleted successfully.")
        else:
            print("File does not exist.")

        # Create the API client
        k8s_client = client.CoreV1Api()

        # Get the list of namespaces for the cluster
        namespaces = k8s_client.list_namespace().items

        # Raw namespace json file received as response from azure
        for rawNamespace in namespaces:
            rawNamespace_details = {
                'Cluster_Name': cluster[cicdGitlabConfig.NODE_AKS_NAME],
                'Namespace_Name': rawNamespace.metadata.name, 
                'Contract_ID':  rawNamespace.metadata.labels.get(cicdGitlabConfig.LABEL_CONTRACT_ID)
            }
            raw_namespace_json.append(rawNamespace_details)
        
        raw_output_file = cicdGitlabConfig.ARTIFACTS_RAW_DATA_PATH + cicdGitlabConfig.RAW_NAMESPACE_FILE_NAME.format(cluster[cicdGitlabConfig.NODE_AKS_NAME],datetime.datetime.today().strftime("%Y%m%d"))
        #raw_output_file = "raw_"+cluster[config.NODE_AKS_NAME]+'.json'
        with open(raw_output_file, 'w') as file:
            json.dump(raw_namespace_json, file)
        
        # Compile namespaces data based on billing flag
        filterNamespaces = [namespace for namespace in namespaces if cicdGitlabConfig.LABEL_VALUE_OF_BILLING in namespace.metadata.labels and namespace.metadata.labels.get(cicdGitlabConfig.LABEL_VALUE_OF_BILLING, 0) in valueOfBillingConfig]

        # Iterate over each namespace and extract details
        for namespace in filterNamespaces:
            namespace_details = {
                'Cluster_Name': cluster[cicdGitlabConfig.NODE_AKS_NAME],
                'Namespace_Name': namespace.metadata.name, 
                'Value_Of_Billing': valueOfBillingConfig.get(namespace.metadata.labels[cicdGitlabConfig.LABEL_VALUE_OF_BILLING]),
                'Contract_ID':  namespace.metadata.labels.get(cicdGitlabConfig.LABEL_CONTRACT_ID)
            } 
            print(namespace_details)
            compline_namespaces_json.append(namespace_details)
            
    if compline_namespaces_json is None or len(compline_namespaces_json) == 0:
       raise ValueError("No Namespaces are billable on AKS") 
    else:
        compile_aks_output_file = cicdGitlabConfig.ARTIFACTS_COMPILE_DATA_PATH + cicdGitlabConfig.NAMESPACE_FILE_NAME.format(datetime.datetime.today().strftime("%Y%m%d"))
        with open(compile_aks_output_file, 'w') as file:
            json.dump(compline_namespaces_json, file)
        
@log_exception()
def GetAKSClient():
    # Authenticate using service principal credentials
    credentials = ClientSecretCredential(
        tenant_id=os.environ.get(cicdGitlabConfig.VAR_AZURE_TENANT_ID),
        client_id=os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_ID),
        client_secret=os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_SECRET)
    )
    
    # Instantiate the AKS client
    aks_client = ContainerServiceClient(credentials, os.environ.get(cicdGitlabConfig.VAR_AZURE_SUBSCRIPTION_ID))
    return aks_client

def main():
    GetAKSNamespaceDetails() 

if __name__ == "__main__":
    main()
