# invoice-billing-generator

## README.md

This repository contains code for performing various operations related to Azure Kubernetes Service (AKS) namespaces using GitLab CI/CD pipeline.

### Pipeline Stages

The `.gitlab-ci.yml` file defines the following stages in the pipeline:

1. **ArtifactFolderStructure**:
   - Creates the required folder structure for the InvoiceGenerator artifact.
   - Directories created: `InvoiceGenerator/InvoiceCSV`, `InvoiceGenerator/CompileData`, `InvoiceGenerator/RawData`, and `InvoiceGenerator/Log`.
   - The `InvoiceGenerator/` folder is included as an artifact.

2. **Prerequisite**:
   - Installs prerequisite packages required for the pipeline.
   - Uses the `python:3.9` Docker image.
   - Updates `pip` to the latest version.
   - Checks if the `InvoiceGenerator/site-packages/` directory already exists.
     - If it exists, displays a message indicating that the artifact is already present.
     - If it doesn't exist, downloads the artifact using a secure file URL.
   - Installs the following packages using `pip`:
     - `kubernetes==26.1.0`
     - `azure-identity==1.13.0`
     - `azure-mgmt-containerservice==24.0.0`
   - Copies the required files from `/usr/local/lib/python3.9/site-packages/` and `.secure_files/` to `InvoiceGenerator/`.
   - The `InvoiceGenerator/` folder is included as an artifact.

3. **AKSNamespaces**:
   - Retrieves and compiles AKS namespaces using the `AKSOperations.py` script.
   - Uses the `python:3.9` Docker image.
   - Copies the `InvoiceGenerator/site-packages/` directory to `/usr/local/lib/python3.9/`.
   - Lists the content of `/usr/local/lib/python3.9/site-packages/`.
   - Executes the `AKSOperations.py` script, which performs the following actions:
     - Reads the JSON config file from the secured file location.
     - Gets the AKS client object.
     - Retrieves all clusters in the resource group.
     - Extracts all namespaces and labels for each cluster.
     - Writes the raw namespace JSON file and compiled namespaces data to respective files in the `InvoiceGenerator/RawData/` and `InvoiceGenerator/CompileData/` directories.
   - Removes the `InvoiceGenerator/site-packages/` and `InvoiceGenerator/.secure_files` directories.
   - The `InvoiceGenerator/` folder is included as an artifact.

4. **Cleanup**:
   - Performs cleanup tasks.
   - Currently, it only echoes "Cleanup".

### Code Files

The repository includes the following code files:

- **AKSOperations.py**:
  - Contains functions for retrieving AKS namespace details using the Azure SDK and Kubernetes Python client.
  - Reads the JSON config file from the secured file location.
  - Retrieves AKS client object using service principal credentials.
  - Retrieves and compiles AKS namespaces based on billing flags.
  - Writes the raw namespace JSON file and compiled namespaces data to respective files.
  - Logs error messages to `InvoiceGenerator/Log/trace.log` using a custom logger.
  - The main entry point is the `main()` function.

- **configuration.py**:
  - Defines the configuration variables used in `AKSOperations.py`.
  - Contains the `GitlabConfig` class with various configuration keys.

- **exception_helper.py**:
  - Contains helper functions for handling exceptions and setting up the logger.
  - The `handle_exception()` decorator wraps the functions to catch and log exceptions.
  - The `setup_logger()` function sets up a logger that logs messages

 to `InvoiceGenerator/Log/trace.log`.

- **helper.py**:
  - Contains helper functions used in the pipeline code.
  - The `ReadJSONFile()` function reads a JSON file and loads it into a Python dictionary.
  - The `getConfigVarValue()` function retrieves configuration variable values based on the environment.
  - The `convertJSONToCsvFile()` function converts JSON data to CSV format and writes it to a given location.

### Usage

To use this code, follow these steps:

1. Set up your GitLab CI/CD pipeline and configure the required variables.
2. Copy the code files (`AKSOperations.py`, `configuration.py`, `exception_helper.py`, and `helper.py`) into your GitLab repository.
3. Update the necessary configurations in `AKSOperations.py` and `configuration.py`.
4. Configure the pipeline stages, dependencies, and artifacts in `.gitlab-ci.yml` as per your requirements.
5. Run the pipeline and monitor the job logs.

Make sure to install the required dependencies (`kubernetes`, `azure-identity`, `azure-mgmt-containerservice`) mentioned in the `Prerequisite` stage before running the pipeline.

For any issues or questions, please refer to the GitLab documentation or reach out to the repository maintainers.