import logging
import logging.handlers
import sys

import traceback
import sys
# Configure the logger
logging.basicConfig(filename='error.log', level=logging.ERROR, format='%(asctime)s %(levelname)s: %(message)s')

def log_exception():
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                trace = traceback.extract_tb(exc_traceback)
                formatted_trace = traceback.format_list(trace)
                exception_info = {
                    'type': str(exc_type),
                    'message': str(exc_value),
                    'stack_trace': formatted_trace
                }
                logging.error(exception_info) 
                sys.exit(1)
        return wrapper
    return decorator