import requests,json
import configuration as ConfigConnection 
cicdAriaToConfig = ConfigConnection.AriaToConfig
cicdGitlabConfig = ConfigConnection.GitlabConfig
import helper
from datetime import datetime as dt
from exception_handling_local import log_exception


@log_exception()
def main():
    aks_billable_ns_filepath = cicdGitlabConfig.ARTIFACTS_COMPILE_DATA_PATH + cicdGitlabConfig.NAMESPACE_FILE_NAME.format(dt.today().strftime("%Y%m%d"))
    retrive_resource_quota_from_aria(aks_billable_ns_filepath) 

@log_exception()
def retrive_resource_quota_from_aria(aks_billable_ns_filepath):
  
  billable_namespaces = helper.ReadJSONFileFromCurrentDirectory("dev-pre-data.json")

  month_offset = int(helper.getConfigVarValue(cicdAriaToConfig.VAR_ARIA_MONTH_OFFSET))
   
  startdatetimestamp,enddatetimestamp,start_date,end_date = helper.get_start_end_dates(month_offset)

  print(startdatetimestamp,enddatetimestamp,start_date,end_date)

  headers = {
    'Authorization': 'Bearer '+ helper.getConfigVarValue(cicdAriaToConfig.VAR_ARIA_TO_TOKEN),
  }
  resourceQuotaMonthlyJson=[]
  resourceQuotaDailyJson=[]
  for billable_namespace in billable_namespaces:

    billable_namespace['Cluster_Name'] =  helper.getConfigVarValue(cicdAriaToConfig.VAR_ARIA_NAMESPACE_PREFIX) + billable_namespace['Cluster_Name']

    queryString = cicdAriaToConfig.ARIA_API_QUERY.format(billable_namespace['Cluster_Name'], billable_namespace['Namespace_Name'],billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'])

    param = {       
      'n' : 'memory',
      'q' : queryString,
      'queryType' : 'WQL',
      's' :  startdatetimestamp,
      'e' : enddatetimestamp,
      'g' : 'h',
      'summarization' : 'MAX',
      'strict' : 'true',
      'view': 'METRIC',
      'includeObsoleteMetrics': 'true',
      'sorted': 'false',
      'cached': 'true',
      'useRawQK': 'false'
    }
    response = requests.get(helper.getConfigVarValue(cicdAriaToConfig.VAR_ARIA_API_URL), headers=headers, params=param)
    if(response.status_code != 200):
       raise ValueError(response.text)
    else:
      #get resourse quota utilization datewise
      raw_aria_api_data = json.dumps(response.json())
      raw_aria_api_dataFileName = cicdGitlabConfig.ARTIFACTS_RAW_DATA_PATH + cicdAriaToConfig.RAW_ARIA_NAMESPACE_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],dt.today().strftime("%Y%m%d"))
      
      with open(raw_aria_api_dataFileName, 'w') as file:
            json.dump(response.json(), file)

      # Region 2: Save data in Details format

      resourceQuotaHourlyJson=[]
      resourceQuotaHourly= helper.calculate_sum_by_hour(raw_aria_api_data)
      resourceQuotaHourlyFileName = cicdGitlabConfig.ARTIFACTS_COMPILE_DATA_PATH + cicdAriaToConfig.DETAIL_ARIA_NAMESPACE_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],dt.today().strftime("%Y%m%d"))
      if resourceQuotaHourly is None or len(resourceQuotaHourly) == 0:
        print(resourceQuotaHourly)
      else:
        
        for resourceQuotaH in resourceQuotaHourly:
            complie_resource_quota_details ={
                'Contract ID': billable_namespace['Contract_ID'],
                'Cluster Name': billable_namespace['Cluster_Name'],
                'Name Space': billable_namespace['Namespace_Name'],  
                'Unit Price(yen)':  billable_namespace['Value_Of_Billing'],
                'Start time (JST)': resourceQuotaH['StartTime'],
                'End Time (JST)': resourceQuotaH['EndTime'],
                'Quotas': resourceQuotaH['Usage'],
                'Cost(yen)': resourceQuotaH['Usage'] * billable_namespace['Value_Of_Billing']
            }
            resourceQuotaHourlyJson.append(complie_resource_quota_details)
        print('Hourly Data : \n')    
        print(resourceQuotaHourlyJson)
        with open(resourceQuotaHourlyFileName, 'w') as file:
          json.dump(resourceQuotaHourlyJson, file)
        print(resourceQuotaHourlyFileName)
        # if resourceQuotaHourlyJson is None or len(resourceQuotaHourlyJson) == 0:
        #  print(resourceQuotaHourlyJson)
        # else:
        #   resourceQuotaHourlyCSVFileName = cicdGitlabConfig.ARTIFACTS_COMPILE_DATA_PATH + cicdAriaToConfig.DETAIL_ARIA_NAMESPACE_CSV_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],dt.today().strftime("%Y%m%d"))
        #   #Region 3: Convert Json to CSV file
        #   helper.convertJSONToCsvFile(resourceQuotaHourlyJson,resourceQuotaHourlyCSVFileName)

      # Region 2: Save data in Header format 
      resourceQuotaDailyJson=[]
      resourceQuotaDaily = helper.calculate_sum_by_date(raw_aria_api_data)
      if resourceQuotaDaily is None  or len(resourceQuotaDaily) == 0:
        print(resourceQuotaDaily)
      else:
        for resourceQuotaD in resourceQuotaDaily:
            complie_resource_quota_details ={
                'Contract ID': billable_namespace['Contract_ID'],
                'Cluster Name': billable_namespace['Cluster_Name'],
                'Name Space': billable_namespace['Namespace_Name'],  
                'Unit Price(yen)':  billable_namespace['Value_Of_Billing'], 
                'Date': resourceQuotaD['Date'],
                'Total Quotas': resourceQuotaD['Usage'],
                'Total Cost(yen)': resourceQuotaD['Usage'] * billable_namespace['Value_Of_Billing']
            }
            resourceQuotaDailyJson.append(complie_resource_quota_details)

     # Region 2: Save data in Header format - Monthwise

      resourceQuotaMonthly = helper.perform_monthly_calculation(resourceQuotaDailyJson)
      print('Month Data : \n')    
      if resourceQuotaMonthly is None  or len(resourceQuotaMonthly) == 0:
        print(resourceQuotaMonthly)
      else:
        for resourceQuotaD in resourceQuotaMonthly:
            complie_resource_quota_details ={
                'Contract ID': billable_namespace['Contract_ID'],
                'Cluster Name': billable_namespace['Cluster_Name'],
                'Name Space': billable_namespace['Namespace_Name'],
                'Unit Price(yen)':  billable_namespace['Value_Of_Billing'],
                'Start Date (JST)': start_date,
                'End Date (JST)': end_date,
                'Total Quotas':  resourceQuotaD['Usage'],
                'Total Cost(yen)': billable_namespace['Value_Of_Billing'] * resourceQuotaD['Usage']
            }
            resourceQuotaMonthlyJson.append(complie_resource_quota_details)
  print(resourceQuotaMonthlyJson)
  # #Store monthwise data
  if resourceQuotaMonthlyJson is None  or len(resourceQuotaMonthlyJson) == 0:
    print(resourceQuotaMonthlyJson)
  else:
    resourceQuotaMonthlyFileName =   cicdGitlabConfig.ARTIFACTS_COMPILE_DATA_PATH +  cicdAriaToConfig.HEADER_ARIA_NAMESPACE_FILE_NAME.format(dt.today().strftime("%Y%m%d"))
    with open(resourceQuotaMonthlyFileName, 'w') as file:
      json.dump(resourceQuotaMonthlyJson, file)
    
    # resourceQuotaMonthlyCSVFileName =  cicdGitlabConfig.ARTIFACTS_COMPILE_DATA_PATH + cicdAriaToConfig.HEADER_ARIA_NAMESPACE_CSV_FILE_NAME.format(dt.today().strftime("%Y%m%d"))
    # #Region 3: Convert Json to CSV file
    # helper.convertJSONToCsvFile(resourceQuotaMonthlyJson,resourceQuotaMonthlyCSVFileName)
     

if __name__ == "__main__":
    main()
