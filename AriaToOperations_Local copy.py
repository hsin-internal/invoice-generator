import requests,json
import configuration_local as LocalConfigConnection
import os
cicdAriaToConfig = LocalConfigConnection.AriaToConfig
import helper
import datetime 
from exception_handling_local import log_exception

@log_exception()
def retrive_resource_quota_from_aria():

  billable_namespaces = helper.ReadJSONFileFromCurrentDirectory("dev-pre-data.json")
  
  startdatetimestamp,enddatetimestamp = helper.get_start_end_dates(cicdAriaToConfig.VAR_ARIA_MONTH_OFFSET)

  headers = {
    'Authorization': 'Bearer '+ cicdAriaToConfig.VAR_ARIA_TO_TOKEN,
  }
  resourceQuotaMonthlyJson=[]
  resourceQuotaDailyJson=[]
  for billable_namespace in billable_namespaces:

    #queryString = 'if(ceil(align(1h,mean,rawsum(align(5m,mean,(ts("kubernetes.ns.memory.request", cluster="' + billable_namespace['Cluster_Name'] + '" and namespace_name="' + billable_namespace['Namespace_Name']+ '")))))/1024/1024/1024/5)>ceil(align(1h,mean, rawsum(align(5m,mean,(ts("kubernetes.ns.cpu.request", cluster="'+billable_namespace['Cluster_Name']+'" and namespace_name="'+billable_namespace['Namespace_Name']+ '")))))/1000),ceil(align(1h,mean,rawsum(align(5m,mean,(ts("kubernetes.ns.memory.request", cluster="' +billable_namespace['Cluster_Name'] + '" and namespace_name="'+ billable_namespace['Namespace_Name'] + '")))))/1024/1024/1024/5),ceil(align(1h,mean, rawsum(align(5m,mean,(ts("kubernetes.ns.cpu.request", cluster="' +billable_namespace['Cluster_Name'] +'" and namespace_name="' +billable_namespace['Namespace_Name']+'")))))/1000))'
    queryString = cicdAriaToConfig.ARIA_API_QUERY.format(billable_namespace['Cluster_Name'], billable_namespace['Namespace_Name'],billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'])

    param = {       
      'n' : 'memory',
      'q' : queryString,
      'queryType' : 'WQL',
      's' :  startdatetimestamp,
      'e' : enddatetimestamp,
      'g' : 'h',
      'summarization' : 'MAX',
      'strict' : 'true'
    }

    response = requests.get(cicdAriaToConfig.ARIA_API_URL, headers=headers, params=param)

    #get resourse quota utilization datewise
    raw_aria_api_data = json.dumps(response.json())
    raw_aria_api_dataFileName = cicdAriaToConfig.RAW_ARIA_NAMESPACE_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],datetime.date.today().strftime("%Y%m%d"))
    
    with open(raw_aria_api_dataFileName, 'w') as file:
          json.dump(response.json(), file)

    # Region 2: Save data in Details format

    resourceQuotaHourlyJson=[]
    resourceQuotaHourly= helper.calculate_sum_by_hour(raw_aria_api_data)
    resourceQuotaHourlyFileName = cicdAriaToConfig.DETAIL_ARIA_NAMESPACE_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],datetime.date.today().strftime("%Y%m%d"))
    if resourceQuotaHourly is None or len(resourceQuotaHourly) == 0:
      print(resourceQuotaHourly)
    else:
      for resourceQuotaH in resourceQuotaHourly:
          complie_resource_quota_details ={
              'CONTRACT_ID': billable_namespace['Contract_ID'],
              'CLUSTER_NAME': billable_namespace['Cluster_Name'],
              'NAMESPACE_NAME': billable_namespace['Namespace_Name'], 
              'KEY_OF_BILLING':  billable_namespace['Key_Of_Billing'],
              'VALUE_OF_BILLING':  billable_namespace['Value_Of_Billing'],
              'TIME_STAMP': resourceQuotaH['Date'],
              'QUOTA_USAGE': resourceQuotaH['Usage'],
              'TOTAL_BILL_AMOUNT': billable_namespace['Value_Of_Billing'] * resourceQuotaH['Usage']
          }
          resourceQuotaHourlyJson.append(complie_resource_quota_details)
      
      with open(resourceQuotaHourlyFileName, 'w') as file:
        json.dump(resourceQuotaHourlyJson, file)
      
      print(resourceQuotaHourlyJson)
      #Region 3: Convert Json to CSV file
      resourceQuotaHourlyCSVFileName = cicdAriaToConfig.DETAIL_ARIA_NAMESPACE_CSV_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],datetime.date.today().strftime("%Y%m%d"))
        #Region 3: Convert Json to CSV file
      helper.convertJSONToCsvFile(resourceQuotaHourlyJson,resourceQuotaHourlyCSVFileName)
      # Region 2: Save data in Header format
      
      #Region 3: Convert Json to CSV file
      resourceQuotaHourlyCSVFileName = cicdAriaToConfig.DETAIL_ARIA_NAMESPACE_CSV_FILE_NAME.format(billable_namespace['Cluster_Name'],billable_namespace['Namespace_Name'],datetime.date.today().strftime("%Y%m%d"))
        #Region 3: Convert Json to CSV file
      helper.convertJSONToCsvFile(resourceQuotaHourlyJson,resourceQuotaHourlyCSVFileName)
  # Region 2: Save data in Header format - Daywise
      resourceQuotaDailyJson=[]
      resourceQuotaDaily = helper.calculate_sum_by_date(raw_aria_api_data)
      if resourceQuotaHourly is None  or len(resourceQuotaDaily) == 0:
        print(resourceQuotaHourly)
      else:
        for resourceQuotaD in resourceQuotaDaily:
            complie_resource_quota_details ={
                'CONTRACT_ID': billable_namespace['Contract_ID'],
                'CLUSTER_NAME': billable_namespace['Cluster_Name'],
                'NAMESPACE_NAME': billable_namespace['Namespace_Name'],
                'KEY_OF_BILLING':  billable_namespace['Key_Of_Billing'],
                'VALUE_OF_BILLING':  billable_namespace['Value_Of_Billing'],
                'TIME_STAMP': resourceQuotaD['Date'],
                'QUOTA_USAGE': resourceQuotaD['Usage'],
                'TOTAL_BILL_AMOUNT': billable_namespace['Value_Of_Billing'] * resourceQuotaD['Usage']
            }
            resourceQuotaDailyJson.append(complie_resource_quota_details)

      # Region 2: Save data in Header format - Monthwise

      resourceQuotaMonthly = helper.perform_monthly_calculation(resourceQuotaDaily)
 
      if resourceQuotaHourly is None  or len(resourceQuotaMonthly) == 0:
        print(resourceQuotaHourly)
      else:
        for resourceQuotaD in resourceQuotaMonthly:
            complie_resource_quota_details ={
                'CONTRACT_ID': billable_namespace['Contract_ID'],
                'CLUSTER_NAME': billable_namespace['Cluster_Name'],
                'NAMESPACE_NAME': billable_namespace['Namespace_Name'],
                'KEY_OF_BILLING':  billable_namespace['Key_Of_Billing'],
                'VALUE_OF_BILLING':  billable_namespace['Value_Of_Billing'],
                'TIME_STAMP': resourceQuotaD['Month'],
                'QUOTA_USAGE': resourceQuotaD['Usage'],
                'TOTAL_BILL_AMOUNT': billable_namespace['Value_Of_Billing'] * resourceQuotaD['Usage']
            }
            resourceQuotaMonthlyJson.append(complie_resource_quota_details)

    #Store Daywise data
    
    if resourceQuotaDailyJson is None  or len(resourceQuotaDailyJson) == 0:
       print(resourceQuotaDailyJson)
    else:
      resourceQuotaDailyFileName = cicdAriaToConfig.HEADER_DAILY_ARIA_NAMESPACE_FILE_NAME.format(datetime.date.today().strftime("%Y%m%d"))
      with open(resourceQuotaDailyFileName, 'w') as file:
            json.dump(resourceQuotaDailyJson, file) 
      
      resourceQuotaDailyCSVFileName = cicdAriaToConfig.HEADER_DAILY_ARIA_NAMESPACE_CSV_FILE_NAME.format(datetime.date.today().strftime("%Y%m%d"))
      #Region 3: Convert Json to CSV file
      helper.convertJSONToCsvFile(resourceQuotaDailyJson,resourceQuotaDailyCSVFileName)

    #Store monthwise data
    if resourceQuotaMonthlyJson is None  or len(resourceQuotaMonthlyJson) == 0:
      print(resourceQuotaMonthlyJson)
    else:
      resourceQuotaMonthlyFileName = cicdAriaToConfig.HEADER_ARIA_NAMESPACE_FILE_NAME.format(datetime.date.today().strftime("%Y%m%d"))
      with open(resourceQuotaMonthlyFileName, 'w') as file:
        json.dump(resourceQuotaMonthlyJson, file)
      
      resourceQuotaMonthlyCSVFileName = cicdAriaToConfig.HEADER_ARIA_NAMESPACE_CSV_FILE_NAME.format(datetime.date.today().strftime("%Y%m%d"))
      #Region 3: Convert Json to CSV file
      helper.convertJSONToCsvFile(resourceQuotaMonthlyJson,resourceQuotaMonthlyCSVFileName)


def main():
    retrive_resource_quota_from_aria() 

if __name__ == "__main__":
    main()
