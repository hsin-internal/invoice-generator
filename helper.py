import json
import os
import csv  
from datetime import datetime as dt, timedelta,timezone
import pytz
import calendar 

#read json file from given location and load it to python dictionary
def ReadJSONFile(Filelocation):
    if Filelocation is None:
        return None
    with open(Filelocation) as file:
        # Read the file contents
        json_str = file.read()
        JSON_File = json.loads(json_str)
    return JSON_File

def ReadJSONFileFromCurrentDirectory(Filelocation):
    # Get the current directory path
    current_dir = os.getcwd()

    # Construct the file path by joining the current directory path and the Filelocation
    file_path = os.path.join(current_dir, Filelocation)

    # Check if the file exists
    if not os.path.exists(file_path):
        raise FileNotFoundError(f"JSON file '{Filelocation}' not found in the current directory.")

    # Read the file and load its contents as JSON
    with open(file_path) as file:
        json_str = file.read()
        json_data = json.loads(json_str)

    return json_data

#
def getConfigVarValue(configKey):
    return os.environ.get(configKey)


#convert json data to csv and write to given location
def convertJSONToCsvFile(JSONdata,CsvFilePath):

    # Extracting keys from the first JSON object to use as CSV header
    header = JSONdata[0].keys()

    # Open the file in write mode and create a CSV writer
    with open(CsvFilePath, 'w', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=header)

        # Write the CSV header
        writer.writeheader()

        # Write the data to the CSV file
        writer.writerows(JSONdata)

    print(f"CSV file '{CsvFilePath}' created successfully.")

#Read artifcats file from desired location
def ReadArtifactsFile(fileLocation): 
    #get current directory    
    current_dir = os.environ.get("CI_PROJECT_DIR") 
    #create file location path for gitlab
    file_path = os.path.join(current_dir, fileLocation) 
    #read given file
    if not os.path.exists(file_path):
        return None
    else:
        with open(file_path, "r") as file: 
            file_str = file.read() 
    
    return file_str


#convert epoch milli second to datetime in format of year-month-date hours-mins-seconds
def convert_epoch_millis_to_datetime(epoch_millis):
   
    # Convert epoch milliseconds to dt
    dt_obj = dt.fromtimestamp(epoch_millis)

    # Format the dt as desired
    formatted_date = dt_obj.strftime("%Y-%m-%d %H:%M:%S")
    
    # Specify the format of the string
    format_string = "%Y-%m-%d %H:%M:%S"

    # Convert string to datetime
    datetime_obj = dt_obj.strptime(formatted_date, format_string)

    return datetime_obj 

#convert epoch milli second to date in format of year-month-date
def convert_epoch_millis_to_date(epoch_millis):
   
    # Convert epoch milliseconds to dt
    dt_obj = dt.fromtimestamp(epoch_millis)

    # Format the dt as desired
    formatted_date = dt_obj.strftime("%Y-%m-%d")
    
    return formatted_date

#convert date in format of JST
def convertUTCDateToJST(utc_datetime):
    # Convert the UTC time to JST
    jst_timezone = pytz.timezone('Japan')
    jst_datetime = utc_datetime.astimezone(jst_timezone)  
    return jst_datetime

#bases on month_0ffset set we will start and end date of month i.e., 0,1,2,3 to current, last month, second last, third last respectively
def get_start_end_dates(month_offset):
    
    today = convertUTCDateToJST(dt.now(timezone.utc))
    print('today :')
    print(today)
    current_month = today.month
    current_year = today.year

    # Calculate the target month and year
    target_month = current_month - month_offset
    target_year = current_year

    if target_month <= 0:
        # Adjust for negative or zero month
        target_month += 12
        target_year -= 1

    # Calculate the start and end dates
    start_date = dt(target_year, target_month, 1)
    if current_month==target_month:
        last_day = dt(target_year, target_month, today.day)
        end_date = last_day - timedelta(days=1)
    else: 
        last_day = dt(target_year, target_month, 1) + timedelta(days=32)
        end_date = last_day.replace(day=1) - dt.timedelta(days=1)

    # Convert to epoch milliseconds
    start_timestamp = int(start_date.timestamp()) * 1000
    end_timestamp = int(end_date.timestamp()) * 1000

    return start_timestamp, end_timestamp,start_date.strftime('%Y-%m-%d %H:%M:%S'),end_date.strftime('%Y-%m-%d %H:%M:%S')


#calculate the resource quota utilization for every hours & convert date in actual format
def calculate_sum_by_hour(json_data):
    # Parse the JSON data
    data = json.loads(json_data)
    if "timeseries" in data:
        # Extract the "data" object
        data_object = data["timeseries"][0]["data"]
        format_string = "%Y-%m-%d %H:%M:%S"
        # Extract the date and usage number from each data point
        result = []
        for data_point in data_object:
            timestamp = data_point[0]
            value = data_point[1]

            # Convert timestamp to date
            date = convert_epoch_millis_to_datetime(timestamp)
            data_dict = {"StartTime": (date - timedelta(hours=1)).strftime(format_string),"EndTime": date.strftime(format_string), "Usage": value}

            # Append the date and usage number to the result list
            result.append((data_dict))

        return result
    else:
        print("Key does not exist in the dictionary")
        return None

#calculate the resource quota utilization for every day & convert date in actual format
def calculate_sum_by_date(json_data):

    usage_by_date = {}
    # Parse the JSON data
    data = json.loads(json_data)
    if "timeseries" in data:
        # Extract the "data" object 
        data_object = data["timeseries"][0]["data"]

        for timestamp, usage in data_object:
            # Convert timestamp to dt
            date_str = convert_epoch_millis_to_date(timestamp)

            # Sum up usage for each date
            if date_str in usage_by_date:
                usage_by_date[date_str] += usage
            else:
                usage_by_date[date_str] = usage 
        # Convert the result to the desired format
        result = [{"Date": date_str, "Usage": usage} for date, usage in usage_by_date.items()]

        return result
    else:
        print("Key does not exist in the dictionary")
        return None

#calculate the resource quota utilization for every month
def perform_monthly_calculation(result):
    monthly_summary = {}

    for data in result:
        date = data["Date"]
        # Specify the format of the string
        format_string = "%Y-%m-%d"

        # Convert string to datetime
        datetime_obj = dt.strptime(date, format_string) 
        
        usage = data["Total Quotas"] 
        year = datetime_obj.year  # Extract the year and month portion from the date
        month_name = calendar.month_name[datetime_obj.month] 
        # Sum up the usage for each month
        if month_name in monthly_summary:
            monthly_summary[month_name] += usage
        else:
            monthly_summary[month_name] = usage

    # Convert the result to the desired format
    monthly_result = [{"Month": str(year) + '-' + month_name, "Usage": usage} for month_name, usage in monthly_summary.items()]

    return monthly_result