FROM ubuntu:latest

# Install necessary packages
RUN apt-get update && apt-get install -y \
    curl \
    python3 \
    python3-pip

# Install Azure CLI
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# Download and install Kubelogin binary
RUN az aks install-cli

# Set up Python packages
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# Set the working directory
WORKDIR /app

# Copy the necessary files to the container
COPY . .

